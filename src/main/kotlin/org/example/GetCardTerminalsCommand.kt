@file:OptIn(ExperimentalCli::class)
package org.example

import de.gematik.ws.conn.connectorcontext.v2.ContextType
import de.gematik.ws.conn.eventservice.v7.GetCardTerminals
import de.gematik.ws.conn.eventservice.wsdl.v7.EventService
import de.gematik.ws.conn.eventservice.wsdl.v7.FaultMessage
import jakarta.xml.ws.BindingProvider
import kotlinx.cli.*

class GetCardTerminalsCommand: Subcommand("getCardTerminals", "Auflistung verfügbarer Kartenterminals") {
    val mandantId by option(ArgType.String, shortName = "m", description = "Id des Mandanten").required()
    val clientId by option(ArgType.String, shortName = "c", description = "Id des Klienten").required()
    val workplaceId by option(ArgType.String, shortName = "w", description = "Id des Arbeitsplatzes").required()
    val userId by option(ArgType.String, shortName = "u", description = "Id des Users")
    val mandantWide by option(ArgType.Boolean, shortName = "mw", description = "Ist diese Option gesetzt, werden die Kartenterminals für alle Arbeitsplätze zurückgegeben, die dem aktuellen Mandanten und Klientensystem zugeordnet sind").default(false)

    override fun execute() {
        val context = ContextType()
        context.mandantId = mandantId
        context.clientSystemId = clientId
        context.workplaceId = workplaceId
        context.userId = userId

        val query = GetCardTerminals()
        query.context = context
        query.isMandantWide = mandantWide

        val port = EventService().eventServicePort
        val bindingProvider = port as BindingProvider
        bindingProvider.requestContext.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, "http://127.0.0.1:80/eventservice")

        try {
            val result = port.getCardTerminals(query)
            val cardTerminals = result.cardTerminals.cardTerminal
            for (t in cardTerminals) {
                println("---- ${t.name} (ID: ${t.ctId}) ----")
                println("Anzahl Slots: ${t.slots}")
                println(if (t.isConnected) "Status: verfügbar" else "Status: nicht verfügbar")
            }
        } catch (f: FaultMessage) {
            println("Something went wrong")
            println(f.faultInfo.trace[0].errorText)
        }
    }
}