@file:OptIn(ExperimentalCli::class)
package org.example

import de.gematik.ws.conn.cardservice.wsdl.v8.CardService
import de.gematik.ws.conn.cardservice.wsdl.v8.FaultMessage
import de.gematik.ws.conn.cardservicecommon.v2.PinResultEnum
import de.gematik.ws.conn.connectorcommon.v5.Status
import de.gematik.ws.conn.connectorcontext.v2.ContextType
import jakarta.xml.ws.BindingProvider
import jakarta.xml.ws.Holder
import kotlinx.cli.ArgType
import kotlinx.cli.ExperimentalCli
import kotlinx.cli.Subcommand
import kotlinx.cli.required
import java.math.BigInteger

class VerifyPinCommand: Subcommand("verifyPin", "Freischaltung der übergebenen Karte") {
    val mandantId by option(ArgType.String, shortName = "m", description = "Id des Mandanten").required()
    val clientId by option(ArgType.String, shortName = "c", description = "Id des Klienten").required()
    val workplaceId by option(ArgType.String, shortName = "w", description = "Id des Arbeitsplatzes").required()
    val userId by option(ArgType.String, shortName = "u", description = "Id des Users")

    val cardHandle by option(ArgType.String, description = "Handle der freizuschaltenden Karte").required()
    val pinType by option(ArgType.Choice(listOf("PIN.SMC", "PIN.CH"), { it }), description = "Art des Pins").required()

    override fun execute() {
        val context = ContextType()
        context.mandantId = mandantId
        context.clientSystemId = clientId
        context.workplaceId = workplaceId
        context.userId = userId

        val port = CardService().cardServicePort
        val bindingProvider = port as BindingProvider
        bindingProvider.requestContext.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, "http://127.0.0.1:80/cardservice")

        try {
            val status = Holder<Status>()
            val pinResult = Holder<PinResultEnum>()
            val leftTries = Holder<BigInteger>()

            port.verifyPin(context, cardHandle, pinType, status, pinResult, leftTries)

            println("Ergebnis der PIN-Eingabe: ${pinResult.value.value()}")
            if (pinResult.value == PinResultEnum.REJECTED) {
                println("verbleibende Versuche: ${leftTries.value}")
            }
        } catch (f: FaultMessage) {
            println("Something went wrong")
            println(f.faultInfo.trace[0].errorText)
        }
    }
}