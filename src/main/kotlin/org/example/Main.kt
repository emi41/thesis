@file:OptIn(ExperimentalCli::class)
package org.example

import kotlinx.cli.ArgParser
import kotlinx.cli.ExperimentalCli

fun main(args: Array<String>) {
    val parser = ArgParser("testPrimaersystem")

    val readVSDCommand = ReadVSDCommand()
    val getCardsCommand = GetCardsCommand()
    val verifyPinCommand = VerifyPinCommand()
    val getCardTerminalsCommand = GetCardTerminalsCommand()
    val requestCardCommand = RequestCardCommand()

    parser.subcommands(getCardsCommand, readVSDCommand, getCardTerminalsCommand, verifyPinCommand, requestCardCommand)
    parser.parse(args)
}