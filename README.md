# Kommandozeilenapplikation zum vereinfachten Zugriff auf einige Schnittstellen des Konnektors

## Nutzung

- Klonen des Repositories
- Ersetzen des Ordners `schemaDateien` in `src/main/resources/META-INF` durch den Ordner mit den Schemadateien für 
das Release 3.1.2. Diese sind zu finden unter <https://fachportal.gematik.de/downloadcenter/historie>. Dort liegen die als ZIP verpackten Schemata für das entsprechende Release
unter **Schemata (ZIP)**.
- Anlegen eines Ordners `src/test/kotlin` für mögliche Tests.
- Unter Umständen Anpassen der Adressen der Endpunkte für die benötigten Dienste an die Angaben im
Diensteverzeichnis des Konnektor(-simulators). Im Kotlin-Code wird das bereits über das BindingProvider-Objekt mit den Default-Werten des Konnektorsimulators gemacht.
- Ausführen von Maven Install (unter Nutzung eines JDK 11) 
- Ausführen der samt Abhängigkeiten gebündelten jar: `
java -jar -Dfile.encoding=UTF-8 target/consoleApp-1.0-SNAPSHOT-jar-with-dependencies.jar -h
`

## Besonderheiten

Als Parser für die Argumente auf der Kommandozeile wird `kotlinx-cli` genutzt (<https://github.com/Kotlin/kotlinx-cli>).
Diese Bibliothek ist noch nicht als stabiles Release veröffentlicht und kann sich daher noch stärker verändern.

Bis zu den Aufrufen der eigentlichen Operationen in den Subkommandos lässt sich dieses Programm auch ohne laufenden Konnektor(-simulator) nutzen. Aus fachlicher
Perspektive macht das allerdings wenig Sinn.

## Lizens

Dieses Projekt ist lizensiert unter der MIT License. Die volle Lizens findet sich in der `LICENSE`-Datei auf der obersten Ebene in der Dateienhierarchie dieses Projekts.


