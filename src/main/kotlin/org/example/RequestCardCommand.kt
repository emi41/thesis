@file:OptIn(ExperimentalCli::class)
package org.example

import de.gematik.ws.conn.cardservice.v8.CardInfoType
import de.gematik.ws.conn.cardservicecommon.v2.CardTypeType
import de.gematik.ws.conn.cardterminalservice.v1.Slot
import de.gematik.ws.conn.cardterminalservice.wsdl.v1.CardTerminalService
import de.gematik.ws.conn.cardterminalservice.wsdl.v1.FaultMessage
import de.gematik.ws.conn.connectorcommon.v5.Status
import de.gematik.ws.conn.connectorcontext.v2.ContextType
import jakarta.xml.ws.BindingProvider
import jakarta.xml.ws.Holder
import kotlinx.cli.*
import java.math.BigInteger

class RequestCardCommand: Subcommand("requestCard", "Rückgabe von Karteninformationen für bestimmten Slot") {
    val mandantId by option(ArgType.String, shortName = "m", description = "Id des Mandanten").required()
    val clientId by option(ArgType.String, shortName = "c", description = "Id des Klienten").required()
    val workplaceId by option(ArgType.String, shortName = "w", description = "Id des Arbeitsplatzes").required()
    val userId by option(ArgType.String, shortName = "u", description = "Id des Users")

    val cardTerminalId by option(ArgType.String, shortName = "ctId", description = "Id des Kartenterminals").required()
    val slotId by option(ArgType.Int, shortName = "sId", description = "Nummer des Slots des angegebenen Kartenterminals (gezählt wird ab 1)").required()

    val cardType by option(ArgType.Choice<CardTypeType>(toString = { cardTypeType -> cardTypeType.value()}, toVariant = CardTypeType::fromValue), shortName = "C", description = "Filter nach Kartentyp")
    val timeout by option(ArgType.Int, shortName = "t", description = "Sekunden, die der Konnektor auf Stecken der Karte maximal wartet").default(20)

    override fun execute() {
        val context = ContextType()
        context.mandantId = mandantId
        context.clientSystemId = clientId
        context.workplaceId = workplaceId
        context.userId = userId

        val slot = Slot()
        slot.ctId = cardTerminalId
        slot.slotId = BigInteger.valueOf(slotId.toLong())

        val port = CardTerminalService().cardTerminalServicePort
        val bindingProvider = port as BindingProvider
        bindingProvider.requestContext.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, "http://127.0.0.1:80/cardterminalservice")

        try {
            val status = Holder<Status>()
            val alreadyInserted = Holder<Boolean>()
            val card = Holder<CardInfoType>()

            port.requestCard(context, slot, cardType, "", BigInteger.valueOf(timeout.toLong()), status, alreadyInserted, card)
            if (card.value != null) {
                println("---- Kartentyp (Typ: ${card.value.cardType.value()}) ----")
                println("Card Handle: ${card.value.cardHandle}")
                println("Kartenterminal: ${card.value.ctId} (Slot ${card.value.slotId})")
                if (card.value.cardType != CardTypeType.KVK && card.value.cardType != CardTypeType.UNKNOWN) {
                    println("Kartenhalter: ${card.value.cardHolderName}")
                }
                if (alreadyInserted.value) {
                    println("(Karte steckte bereits!)")
                }
            } else {
                println("Keine Karte gesteckt!")
            }
        } catch (f: FaultMessage) {
            println("Something went wrong")
            println(f.faultInfo.trace[0].errorText)
        }
    }
}

