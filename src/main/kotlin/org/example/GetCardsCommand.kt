@file:OptIn(ExperimentalCli::class)
package org.example

import de.gematik.ws.conn.cardservicecommon.v2.CardTypeType
import de.gematik.ws.conn.connectorcontext.v2.ContextType
import de.gematik.ws.conn.eventservice.v7.GetCards
import de.gematik.ws.conn.eventservice.wsdl.v7.EventService
import de.gematik.ws.conn.eventservice.wsdl.v7.FaultMessage
import jakarta.xml.ws.BindingProvider
import kotlinx.cli.*
import java.math.BigInteger

class GetCardsCommand: Subcommand("getCards", "Auflistung gesteckter Karten") {
    val mandantId by option(ArgType.String, shortName = "m", description = "Id des Mandanten").required()
    val clientId by option(ArgType.String, shortName = "c", description = "Id des Klienten").required()
    val workplaceId by option(ArgType.String, shortName = "w", description = "Id des Arbeitsplatzes").required()
    val userId by option(ArgType.String, shortName = "u", description = "Id des Users")
    val cardType by option(ArgType.Choice<CardTypeType>(toString = { cardTypeType -> cardTypeType.value()}, toVariant = CardTypeType::fromValue), shortName = "C", description = "Filter nach Kartentyp")
    val mandantWide by option(ArgType.Boolean, shortName = "mw", description = "Ist diese Option gesetzt, werden Karten für alle Arbeitsplätze zurückgegeben, die dem aktuellen Mandanten und Klientensystem zugeordnet sind").default(false)
    val cardTerminalId by option(ArgType.String, shortName = "ctId", description = "Filter nach einem bestimmten Kartenterminal per entsprechender Id")
    val slotId by option(ArgType.Int, shortName = "sId", description = "Nummer des Slots des angegebenen Kartenterminals (gezählt wird ab 1). Ist kein Terminal angegeben, tritt ein Fehler auf")

    override fun execute() {
        val context = ContextType()
        context.mandantId = mandantId
        context.clientSystemId = clientId
        context.workplaceId = workplaceId
        context.userId = userId

        val query = GetCards()
        query.context = context
        query.isMandantWide = mandantWide
        query.cardType = cardType
        if (slotId != null) {
            query.slotId = BigInteger.valueOf(slotId!!.toLong())
        }
        query.ctId = cardTerminalId

        val port = EventService().eventServicePort
        val bindingProvider = port as BindingProvider
        bindingProvider.requestContext.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, "http://127.0.0.1:80/eventservice")

        try {
            val result = port.getCards(query)
            val cards = result.cards.card
            var i = 1
            for (card in cards) {
                println("---- Karte $i (Typ: ${card.cardType.value()}) ----")
                println("Card Handle: ${card.cardHandle}")
                println("Kartenterminal: ${card.ctId} (Slot ${card.slotId})")
                if (card.cardType != CardTypeType.KVK && card.cardType != CardTypeType.UNKNOWN) {
                    println("Kartenhalter: ${card.cardHolderName}")
                }
                i++
            }
        } catch (f: FaultMessage) {
            println("Something went wrong")
            println(f.faultInfo.trace[0].errorText)
        }
    }
}