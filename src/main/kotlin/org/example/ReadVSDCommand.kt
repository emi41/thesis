@file:OptIn(ExperimentalCli::class)
package org.example

import de.gematik.ws.conn.connectorcontext.v2.ContextType
import de.gematik.ws.conn.vsds.vsdservice.v5.FaultMessage
import de.gematik.ws.conn.vsds.vsdservice.v5.VSDService
import de.gematik.ws.conn.vsds.vsdservice.v5.VSDStatusType
import jakarta.xml.ws.BindingProvider
import jakarta.xml.ws.Holder
import kotlinx.cli.*
import java.io.ByteArrayInputStream
import java.io.InputStream
import java.io.StringWriter
import java.util.zip.GZIPInputStream
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.transform.OutputKeys
import javax.xml.transform.TransformerFactory
import javax.xml.transform.dom.DOMSource
import javax.xml.transform.stream.StreamResult

class ReadVSDCommand: Subcommand("readVSD", "Lesen der Versichertenstammdaten") {
    val cardHandleEgk by option(ArgType.String, shortName = "egk", description = "Handle für zu lesende EGK").required()
    val cardHandleAuth by option(ArgType.String, shortName = "auth", description = "Handle für Karte, die für den authentifizierten Zugriff benötigt wird").required()
    val performOnlineCheck by option(ArgType.Boolean).default(false)
    val readOnlineReceipt by option(ArgType.Boolean).default(false)
    val mandantId by option(ArgType.String, shortName = "m", description = "Id des Mandanten").required()
    val clientId by option(ArgType.String, shortName = "c", description = "Id des Klienten").required()
    val workplaceId by option(ArgType.String, shortName = "w", description = "Id des Arbeitsplatzes").required()
    val userId by option(ArgType.String, shortName = "u", description = "Id des Users")

    override fun execute() {
        val context = ContextType()
        context.mandantId = mandantId
        context.clientSystemId = clientId
        context.workplaceId = workplaceId
        context.userId = userId


        val port = VSDService().vsdServicePort
        val bindingProvider = port as BindingProvider
        bindingProvider.requestContext.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, "http://127.0.0.1:80/vsdservice")

        try {
            val persoenlicheVersichertendaten = Holder<ByteArray>()
            val allgemeineVersichertendaten = Holder<ByteArray>()
            val geschuetzteVersichertendaten = Holder<ByteArray>()
            val vsdStatus = Holder<VSDStatusType>()
            val pruefungsnachweis = Holder<ByteArray>()

            port.readVSD(cardHandleEgk, cardHandleAuth, performOnlineCheck, readOnlineReceipt, context,
                persoenlicheVersichertendaten, allgemeineVersichertendaten, geschuetzteVersichertendaten, vsdStatus, pruefungsnachweis)

            val persoenlicheVersichertendatenDecompressed = GZIPInputStream(ByteArrayInputStream(persoenlicheVersichertendaten.value))
            val allgemeineVersichertendatenDecompressed = GZIPInputStream(ByteArrayInputStream(allgemeineVersichertendaten.value))
            val geschuetzteVersichertendatenDecompressed = GZIPInputStream(ByteArrayInputStream(geschuetzteVersichertendaten.value))

            println("Versichertenstammdaten (mehrere XML-Dateien): \n")
            println(xmlInputStreamToPrettyString(persoenlicheVersichertendatenDecompressed))
            println(xmlInputStreamToPrettyString(allgemeineVersichertendatenDecompressed))
            println(xmlInputStreamToPrettyString(geschuetzteVersichertendatenDecompressed))
        } catch (f: FaultMessage) {
            println("Something went wrong")
            println(f.faultInfo.trace[0].errorText)
        }
    }
}

// Hilfsfunktion für Parsen des XML, in Form dessen die Stammdaten zurückgegeben werden, in gut lesbare Strings.
// Gewählte Lösung in Anlehnung an Antworten in https://stackoverflow.com/questions/25864316/pretty-print-xml-in-java-8
// und https://stackoverflow.com/questions/139076/how-to-pretty-print-xml-from-java
// (jeweils aufgerufen am 13.09.2022)
private fun xmlInputStreamToPrettyString(input: InputStream) : String {
    val document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(input)
    val transformerFactory = TransformerFactory.newInstance()
    transformerFactory.setAttribute("indent-number", 4)
    val transformer = transformerFactory.newTransformer()
    transformer.setOutputProperty(OutputKeys.INDENT, "yes")

    val out = StringWriter()
    transformer.transform(DOMSource(document), StreamResult(out))

    return out.toString()
}
